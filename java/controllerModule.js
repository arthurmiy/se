//Import scripts
import * as THREE from "./three.module.js";
import { FBXLoader } from "./loaders/FBXLoader.js";
import { GLTFLoader } from "./loaders/GLTFLoader.js";
import { DRACOLoader } from "./loaders/DRACOLoader.js";
import { OrbitControls } from "./controls/OrbitControls.js";
import { NexusObject } from "./nexus_three.js";

var ENVController = (function (my) {
  /*
    ----------------------------------------------CONFIGURATION PARAMETERS
*/

  //model parameters

  var loaderFBX = new FBXLoader();
  var loaderGltf = new GLTFLoader();
  var dracoLoader;
  var keyAnimations = new Object();
  let clock;

  //3D elements
  var camera, scene, renderer, light; //basic render objects
  //html elements
  var container;
  //animation elements
  var tween; //camera
  var tween2; //3d objs
  var controls;

  var mouse = new THREE.Vector2(); // create once
  var objOne = [];
  var lastOver = "";
  var lastOverObj;
  var over;

  var currentSlide = 0;

  /*
PUBLIC PARAMETERS
  */
  my.cameraFOV = 45;
  my.cameraNear = 10;
  my.cameraFar = 90000;
  my.objectsToLoad = [];
  my.clickableObjs = [];
  my.positionsList = [];
  my.freeCamera = false;

  /* 
  PUBLIC FUNCTIONS
  */
  my.createEnvironment = function () {
    init();
    animate();
  };

  my.getObjectByName = function (name) {
    return scene.getObjectByName(name);
  };
  my.getValidParentName = getValidParentName;
  my.verifyParentName = verifyParentName;
  my.getOpacity = getOpacity;
  my.getEmissiveIntensity = getEmissiveIntensity;
  my.setEmissive = setEmissive;
  my.setOpacity = setOpacity;
  my.animateKeys = animateKeys;
  my.addTextToScene = addTextToScene;
  my.addGlbToScene = addGlbToScene;
  my.addFbxToScene = addFbxToScene;
  my.getCamCurrentPosition = getCamCurrentPosition;
  my.scrollStatus = scrollStatus;
  /*
  PUBLIC CALLBACKS
  */

  my.onSlideChange = function (oldSlide, newSlide) {};

  my.onSlidePositionUpdate = function (scrollInfos) {};

  my.onObjClicked = function (obj) {};

  my.onMouseOverObj = function (obj) {};

  my.onMouseOutOfObj = function (obj) {};

  /*
    ---------------------------------------------------Initialization functions
*/

  // Initialize 3D environment
  function init() {
    //initialize document container
    container = document.createElement("div");
    document.body.appendChild(container);

    //init clock
    clock = new THREE.Clock();

    //init camera
    camera = new THREE.PerspectiveCamera(
      my.cameraFOV,
      window.innerWidth / window.innerHeight,
      my.cameraNear,
      my.cameraFar
    );

    //create the scene
    scene = new THREE.Scene();

    //create the light
    light = new THREE.HemisphereLight(0xffffff, 0x444444);
    light.position.set(0, 500, 200);
    scene.add(light);

    //create renderer
    renderer = new THREE.WebGLRenderer({ antialias: false });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
    container.appendChild(renderer.domElement);

    controls = new OrbitControls(camera, document.documentElement);
    controls.update();


    window.addEventListener("resize", onWindowResize, false);

    loadModelsFromList(my.objectsToLoad, "scene");
  }

  //render loop
  function animate() {
    requestAnimationFrame(animate);
    TWEEN.update();
    //controls.update();
    const mixerUpdateDelta = clock.getDelta();
    for (var m in keyAnimations) {
      keyAnimations[m].mixer.update(mixerUpdateDelta);
    }


    if (my.freeCamera) {
      controls.update();
    } else {
    scrollIt();
    }
    renderer.render(scene, camera);
  }

  function render() {
    Nexus.beginFrame(renderer.getContext());
    renderer.render(scene, camera);
    Nexus.endFrame(renderer.getContext());
  }

  /*
    ------------------------------------------Window events
*/
  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
  }

  /*
    -----------------------------------------------Camera movement
*/
  function move(to) {
    let duration = 100;

    let from = getCamCurrentPosition();
    tween = new TWEEN.Tween(from)
      .to(to, duration)
      .easing(TWEEN.Easing.Linear.None) // TWEEN.Easing.Quadratic.InOut ...
      .onUpdate(function () {
        camera.position.set(from.x, from.y, from.z);
        camera.rotation.set(from.rx, from.ry, from.rz);
        //controls.target.set(from.rx, from.ry, from.rz);
      })
      .start();
  }

  function scrollStatus() {
    let doc = document.getElementById("content");
    let part = Math.floor(doc.scrollTop / doc.clientHeight);
    let tolerance = 30;
    let percentage =
      (100 * (doc.scrollTop - part * doc.clientHeight)) / doc.clientHeight;
    if (percentage < tolerance) {
      percentage = 0;
    } else {
      percentage = (100 * (percentage - tolerance)) / (100 - tolerance);
    }
    return {
      index: part,
      percent: percentage,
    };
  }

  function scrollIt() {
    let scrollInfos = scrollStatus();
    if (currentSlide != scrollInfos.index) {
      my.onSlideChange(currentSlide, scrollInfos.index);
      currentSlide = scrollInfos.index;
    } else {
      //slide update
      my.onSlidePositionUpdate(scrollInfos);
    }

    if (scrollInfos.index + 1 < my.positionsList.length) {
      var result = percentage2position(
        scrollInfos.percent,
        my.positionsList[scrollInfos.index],
        my.positionsList[scrollInfos.index + 1]
      );
      move(result);
    }
  }

  function percentage2position(percentage, from, to) {
    var final = {
      x: 0,
      y: 0,
      z: 0,
      rx: 0,
      ry: 0,
      rz: 0,
    };

    final.x = from.x + (percentage / 100) * (to.x - from.x);
    final.y = from.y + (percentage / 100) * (to.y - from.y);
    final.z = from.z + (percentage / 100) * (to.z - from.z);
    final.rx = from.rx + (percentage / 100) * (to.rx - from.rx);
    final.ry = from.ry + (percentage / 100) * (to.ry - from.ry);
    final.rz = from.rz + (percentage / 100) * (to.rz - from.rz);

    return final;
  }

  function stop() {
    tween.stop();
  }

  function getCamCurrentPosition() {
    return {
      x: camera.position.x,
      y: camera.position.y,
      z: camera.position.z,
      rx: camera.rotation.x,
      ry: camera.rotation.y,
      rz: camera.rotation.z,
    };
  }

  /*
    ---------------------------------------------------------Load functions
*/

  function getLoadFunction(path) {
    if (path.endsWith("fbx")) {
      return addFbxToScene;
    } else if (path.endsWith("nxs")) {
      //todo Nexus
      return addNexusToScene;
    } else if (path.endsWith("glb")) {
      //gltf
      return addGlbToScene;
    } else if (path.endsWith("text")) {
      return addTextToScene;
    }
  }

  function loadModelsFromList(list, placeToAdd) {
    //todo add to group in cases where there is a model to load and children
    list.forEach(function (item) {
      let tmpGroup;
      if (item.children != null) {
        //list of models
        tmpGroup = new THREE.Group();
        tmpGroup.name = item.name;
        scene.add(tmpGroup);
        loadModelsFromList(item.children, item.name);
      }
      if (item.path != null) {
        //load model and apply transformations
        if (tmpGroup != null) {
          //existe grupo e tem modelo pra carregar
          //todo
        } else {
          //só tem modelo pra adicionar
          let loadFunction = getLoadFunction(item.path);
          loadFunction(item, placeToAdd);
        }
      } else {
        //path null -> verify children
      }
    });
  }

  function addFbxToScene(item, placeToAdd) {
    loaderFBX.load(item.path, function (object) {
      object.traverse(function (child) {
        if (child.isMesh) {
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });

      if (placeToAdd == "scene") {
        scene.add(applyDefinedTransformations(item, object));
      } else {
        scene
          .getObjectByName(placeToAdd)
          .add(applyDefinedTransformations(item, object));
      }

      if (item.onLoadAnimation != null) {
        animateKeys([item.onLoadAnimation], item.onLoadAnimation.duration);
      }
    });
  }

  function addNexusToScene(item) {
    var object = new NexusObject(item.path, renderer, render);
    scene.add(object);
  }

  function addGlbToScene(item, placeToAdd) {
    if (dracoLoader == null) {
      dracoLoader = new DRACOLoader();
      dracoLoader.setDecoderPath("./java/loaders/gltf/");
      loaderGltf.setDRACOLoader(dracoLoader);
    }

    loaderGltf.load(
      item.path,
      function (gltf) {
        let object = gltf.scene;

        // object.traverse(function (child) {
        //   if (child.isMesh) {
        //     child.castShadow = true;
        //     child.receiveShadow = true;
        //   }
        // });

        //if animation is present saves data
        /*
        format
        model_name = {
          mixer: Three.Mixer,
          animation1: Mixer.Action
        }
      */
        if (gltf.animations.length > 0) {
          keyAnimations[item.name] = {
            mixer: new THREE.AnimationMixer(object),
          };
          let i;
          for (i = 0; i < gltf.animations.length; i++) {
            keyAnimations[item.name][gltf.animations[i].name] = keyAnimations[
              item.name
            ].mixer.clipAction(gltf.animations[i]);
          }
        }

        if (placeToAdd == "scene") {
          scene.add(applyDefinedTransformations(item, object));
        } else {
          scene
            .getObjectByName(placeToAdd)
            .add(applyDefinedTransformations(item, object));
        }

        if (item.onLoadAnimation != null) {
          animateKeys([item.onLoadAnimation], item.onLoadAnimation.duration);
        }
      },
      function (xhr) {
        console.log((xhr.loaded / xhr.total) * 100 + "% loaded");
      },
      // called when loading has errors
      function (error) {
        console.log("An error happened");
      }
    );
  }

  function CreateAndAddText(
    font,
    size,
    height,
    curveSegments,
    bevelThickness,
    bevelSize,
    bevelEnabled,
    materials,
    item,
    hover,
    placeToAdd
  ) {
    let extracted = item.path.substring(0, item.path.length - ".text".length);
    let textGeo = new THREE.TextGeometry(extracted, {
      font,

      size: size,
      height: height,
      curveSegments: curveSegments,

      bevelThickness: bevelThickness,
      bevelSize: bevelSize,
      bevelEnabled: bevelEnabled,
    });

    textGeo.computeBoundingBox();
    textGeo.computeVertexNormals();

    const triangle = new THREE.Triangle();

    // "fix" side normals by removing z-component of normals for side faces
    // (this doesn't work well for beveled geometry as then we lose nice curvature around z-axis)

    if (!bevelEnabled) {
      const triangleAreaHeuristics = 0.1 * (height * size);

      for (let i = 0; i < textGeo.faces.length; i++) {
        const face = textGeo.faces[i];

        if (face.materialIndex == 1) {
          for (let j = 0; j < face.vertexNormals.length; j++) {
            face.vertexNormals[j].z = 0;
            face.vertexNormals[j].normalize();
          }

          const va = textGeo.vertices[face.a];
          const vb = textGeo.vertices[face.b];
          const vc = textGeo.vertices[face.c];

          const s = triangle.set(va, vb, vc).getArea();

          if (s > triangleAreaHeuristics) {
            for (let j = 0; j < face.vertexNormals.length; j++) {
              face.vertexNormals[j].copy(face.normal);
            }
          }
        }
      }
    }

    const centerOffset =
      -0.5 * (textGeo.boundingBox.max.x - textGeo.boundingBox.min.x);

    textGeo = new THREE.BufferGeometry().fromGeometry(textGeo);

    let object = new THREE.Group();

    let textMesh1 = new THREE.Mesh(textGeo, materials);

    textMesh1.position.x = centerOffset;
    textMesh1.position.y = hover;
    textMesh1.position.z = 0;

    object.add(textMesh1);

    if (item.tProperties.mirror) {
      let textMesh2 = new THREE.Mesh(textGeo, [
        materials[0].clone(),
        materials[1].clone(),
      ]);

      textMesh2.position.x = centerOffset;
      textMesh2.position.y = -hover;
      textMesh2.position.z = height;
      textMesh2.rotation.x = Math.PI;
      textMesh2.rotation.y = Math.PI * 2;
      textMesh2.maxOpacity = item.tProperties.mirrorOpacity;
      setOpacity(textMesh2, item.tProperties.mirrorOpacity);
      object.add(textMesh2);
    }

    return applyDefinedTransformations(item, object);

    if (item.onLoadAnimation != null) {
      animateKeys([item.onLoadAnimation], item.onLoadAnimation.duration);
    }
  }

  function applyDefinedTransformations(item, object) {
    if (item.rotation != null) {
      object.rotation.set(item.rotation[0], item.rotation[1], item.rotation[2]);
    }
    if (item.scale != null) {
      object.scale.set(item.scale[0], item.scale[1], item.scale[2]);
    }
    if (item.position != null) {
      if (item.position.length > 1) {
        //todo insert copies
        var tempGroup = new THREE.Group();
        item.position.forEach((pos) => {
          object.position.set(pos.x, pos.y, pos.z);
          var tmpMesh = object.clone();
          tempGroup.add(tmpMesh);
        });
        tempGroup.name = item.name;
        return tempGroup;
      } else {
        object.name = item.name;
        object.position.set(
          item.position[0].x,
          item.position[0].y,
          item.position[0].z
        );
        return object;
      }
    } else {
      object.name = item.name;
      return object;
    }
  }

  function addTextToScene(item, placeToAdd) {
    //default values
    let fontName = item.tProperties.font ?? "helvetiker";
    let fontWeight = item.tProperties.fontWeight ?? "bold";
    let size = item.tProperties.size ?? 70;
    let height = item.tProperties.height ?? 20;
    let curveSegments = item.tProperties.curveSegments ?? 4;
    let bevelThickness = item.tProperties.bevelThickness ?? 2;
    let bevelSize = item.tProperties.bevelSize ?? 1.5;
    let bevelEnabled = item.tProperties.bevelEnabled ?? true;
    let color = item.tProperties.color ?? 0xffffff;
    let hover = item.tProperties.hover ?? 20;

    //load font
    const Floader = new THREE.FontLoader();
    Floader.load(
      "./fonts/" + fontName + "_" + fontWeight + ".typeface.json",
      function (response) {
        let font = response;

        let materials = [
          new THREE.MeshPhongMaterial({
            color: new THREE.Color(color),
            flatShading: true,
          }), // front
          new THREE.MeshPhongMaterial({ color: new THREE.Color(color) }), // side
        ];

        if (placeToAdd == "scene") {
          scene.add(
            CreateAndAddText(
              font,
              size,
              height,
              curveSegments,
              bevelThickness,
              bevelSize,
              bevelEnabled,
              materials,
              item,
              hover
            )
          );
        } else {
          scene
            .getObjectByName(placeToAdd)
            .add(
              CreateAndAddText(
                font,
                size,
                height,
                curveSegments,
                bevelThickness,
                bevelSize,
                bevelEnabled,
                materials,
                item,
                hover
              )
            );
        }

        if (item.onLoadAnimation != null) {
          animateKeys([item.onLoadAnimation], item.onLoadAnimation.duration);
        }
      }
    );
  }

  /*
    ---------------------------------------------------------Animation control
*/
  function getAnimateKeysInitialState(keys) {
    let initialState = [];

    keys.forEach(function (value) {
      value.poseKeys.forEach(function (pose) {
        //todo only for array props
        if (
          pose.property == "rotation" ||
          pose.property == "position" ||
          pose.property == "scale"
        ) {
          if (pose.target == "children") {
            scene
              .getObjectByName(value.objName)
              .children.forEach(function (child) {
                if (pose.initialState == null) {
                  initialState.push(child[pose.property].toArray());
                } else {
                  initialState.push(pose.initialState);
                }
              });
          } else {
            if (pose.initialState == null) {
              initialState.push(
                scene.getObjectByName(value.objName)[pose.property].toArray()
              );
            } else {
              initialState.push(pose.initialState);
            }
          }
        } else if (pose.property == "Opacity") {
          if (pose.initialState == null) {
            initialState.push(getOpacity(scene.getObjectByName(value.objName)));
          } else {
            initialState.push(pose.initialState);
          }
        } else if (pose.property == "EmissiveIntensity") {
          if (pose.initialState == null) {
            initialState.push(
              getEmissiveIntensity(scene.getObjectByName(value.objName))
            );
          } else {
            initialState.push(pose.initialState);
          }
        }
      });
    });
    return initialState;
  }

  function calculateValueFromPercentage(initial, final, percentage, type) {
    if (type == "absolute") {
      return initial + (final - initial) * percentage;
    } else if (type == "relative") {
      return initial + final * percentage;
    }
  }

  function animateKeys(keys, time) {
    let duration = time ?? 700;
    let initialState = getAnimateKeysInitialState(keys);
    var valueFrom = { perc: 0 };
    var valueTo = { perc: 1 };

    tween2 = new TWEEN.Tween(valueFrom)
      .to(valueTo, duration)
      .easing(TWEEN.Easing.Quadratic.InOut) // TWEEN.Easing.Quadratic.InOut ...
      .onUpdate(function () {
        var counter = 0;
        //search all keys
        keys.forEach(function (value) {
          value.poseKeys.forEach(function (pose) {
            if (
              pose.property == "rotation" ||
              pose.property == "position" ||
              pose.property == "scale"
            ) {
              if (pose.target == "children") {
                scene
                  .getObjectByName(value.objName)
                  .children.forEach(function (child, index) {
                    scene
                      .getObjectByName(value.objName)
                      .children[index][pose.property].set(
                        calculateValueFromPercentage(
                          initialState[counter][0],
                          pose.value[0],
                          valueFrom.perc,
                          pose.type
                        ),
                        calculateValueFromPercentage(
                          initialState[counter][1],
                          pose.value[1],
                          valueFrom.perc,
                          pose.type
                        ),
                        calculateValueFromPercentage(
                          initialState[counter][2],
                          pose.value[2],
                          valueFrom.perc,
                          pose.type
                        )
                      );
                    counter++;
                  });
              } else {
                scene
                  .getObjectByName(value.objName)
                  [pose.property].set(
                    calculateValueFromPercentage(
                      initialState[counter][0],
                      pose.value[0],
                      valueFrom.perc,
                      pose.type
                    ),
                    calculateValueFromPercentage(
                      initialState[counter][1],
                      pose.value[1],
                      valueFrom.perc,
                      pose.type
                    ),
                    calculateValueFromPercentage(
                      initialState[counter][2],
                      pose.value[2],
                      valueFrom.perc,
                      pose.type
                    )
                  );
                counter++;
              }
            } else {
              if (pose.property.toUpperCase() == "Opacity".toUpperCase()) {
                setOpacity(
                  scene.getObjectByName(value.objName),
                  calculateValueFromPercentage(
                    initialState[counter],
                    pose.value,
                    valueFrom.perc,
                    pose.type
                  )
                );
              } else if (
                pose.property.toUpperCase() == "EmissiveIntensity".toUpperCase()
              ) {
                setEmissive(
                  scene.getObjectByName(value.objName),
                  pose.color,
                  calculateValueFromPercentage(
                    initialState[counter],
                    pose.value,
                    valueFrom.perc,
                    pose.type
                  )
                );
              }

              counter++;
            }
          });
        });
      })
      .start();
  }

  /*
          --------------------------------------------------property change
      */

  function setOpacity(obj, opacityIn) {
    let opacity;
    if (obj.maxOpacity != null) {
      if (obj.maxOpacity < opacityIn) {
        opacity = obj.maxOpacity;
      } else {
        opacity = opacityIn;
      }
    } else {
      opacity = opacityIn;
    }
    obj.children.forEach((child) => {
      setOpacity(child, opacity);
    });
    if (obj.material) {
      obj.material.opacity = opacity;
      obj.material.transparent = true;
      if (obj.material.length > 0) {
        obj.material.forEach((child) => {
          child.opacity = opacity;
          child.transparent = true;
        });
        let test = obj.material;
      }
    }
  }

  function setEmissive(obj, emissiveColor, emissiveIntensity) {
    obj.children.forEach((child) => {
      setEmissive(child, emissiveColor, emissiveIntensity);
    });
    if (obj.material) {
      obj.material.emissive = new THREE.Color(emissiveColor ?? 0xff0000);

      if (emissiveIntensity != null) {
        obj.material.emissiveIntensity = emissiveIntensity;
      }
    }
  }

  function getEmissiveIntensity(obj) {
    let tmp, intensity;
    intensity = 1;
    obj.children.forEach((child) => {
      tmp = getEmissiveIntensity(child);
      if (tmp != null) {
        if (tmp < intensity && tmp >= 0) {
          intensity = tmp;
        }
      }
    });
    if (obj.material) {
      return obj.material.emissiveIntensity;
    }
    return intensity;
  }

  function getOpacity(obj) {
    let tmp, opac;
    opac = 1;
    obj.children.forEach((child) => {
      tmp = getOpacity(child);
      if (tmp != null) {
        if (tmp < opac && tmp >= 0) {
          opac = tmp;
        }
      }
    });
    if (obj.material) {
      return obj.material.opacity;
    }
    return opac;
  }
  ///////////////////interaction

  function onDocumentMouseUp(event) {

    //event.preventDefault();

    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

    var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
    var pos = camera.position;
    var ray = new THREE.Raycaster(
      pos,
      vector.unproject(camera).sub(camera.position).normalize()
    );

    var intersects = ray.intersectObjects(objOne.filter(Boolean), true);

    if (intersects.length > 0) {
      my.onObjClicked(intersects[0].object);
      //window.open("./free.html", "_self");
    }
  }

  function getValidParentName(object) {
    let tmp = object.parent.name ?? "-";
    if (tmp == "") {
      return getValidParentName(object.parent);
    } else {
      return tmp;
    }
  }
  function verifyParentName(object, name) {
    let tmp = object.parent;
    if (tmp == null) {
      return false;
    } else {
      if (tmp.name == name) {
        return true;
      } else {
        return verifyParentName(tmp, name);
      }
    }
  }

  function onDocumentMouseMove(event) {
    //event.preventDefault();

    if (objOne.length != my.clickableObjs) {

      objOne = my.clickableObjs.map((obj) => scene.getObjectByName(obj));
    }

    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

    var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
    var pos = camera.position;
    var ray = new THREE.Raycaster(
      pos,
      vector.unproject(camera).sub(camera.position).normalize()
    );

    var intersects = ray.intersectObjects(objOne.filter(Boolean), true);

    let currentName;

    if (intersects.length > 0) {
      //touched
      currentName = intersects[0].object.name;

      if (currentName == "") {
        currentName = getValidParentName(intersects[0].object);
      }

      if (over == false || lastOver != currentName) {
        over = true;
        lastOver = currentName;
        my.onMouseOverObj(intersects[0].object);
        lastOverObj = intersects[0].object;
      }
    } else {
      if (over == true) {
        over = false;
        my.onMouseOutOfObj(lastOverObj);
        lastOver = "";
        lastOverObj = null;
      }
    }
  }

  document.addEventListener("mousedown", onDocumentMouseUp, false);
  document.addEventListener("mousemove", onDocumentMouseMove, false);

  return my;
})(ENVController || {});

export { ENVController };
