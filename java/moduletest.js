import { ENVController } from "./controllerModule.js";

var opened = false;

var clickableObjs = ["towers", "chaves", "abrigo"];

//defines camera pose for each section of the page
var positionsList = [
  {
    x: 3130.124598984848,
    y: 11864.42589490524,
    z: 5178.494277548814,
    rx: -1.2437893525466348,
    ry: 0.48840907237283754,
    rz: 0.9448886199537659,
  },
  {
    x: 2231.9004636363466,
    y: 754.108160420375,
    z: 4135.296735554023,
    rx: -0.21065373096971177,
    ry: 0.7825549212715945,
    rz: 0.14964026939254782,
  },
  {
    x: 183.2033450574188,
    y: 315.3233763657092,
    z: 2171.207619988928,
    rx: -0.2630235935112676,
    ry: 0.8430524995077489,
    rz: 0.19840646728334801,
  },
  {
    x: -464.589191065905,
    y: 270.3371594841066,
    z: 1723.4764435135503,
    rx: -0.1489389814279398,
    ry: -0.03914463714940556,
    rz: -0.005872090768913931,
  },
  {
    x: -474.06202319733455,
    y: 210.09760993769666,
    z: 1658.944354352141,
    rx: -0.11558121730757716,
    ry: -0.0393185770273301,
    rz: -0.004563626597522621,
  },
  {
    x: 2107.6575273302583,
    y: 1093.2316595403058,
    z: -2340.114246239066,
    rx: -2.6476872332266668,
    ry: 1.0444111415036812,
    rz: 2.7058998850456106,
  },
  // {
  //   x: -584.9073826648375,
  //   y: 1039.4807802765056,
  //   z: -2082.4324538944247,
  //   rx: -2.528863468056397,
  //   ry: 0.46301824204467773,
  //   rz: 2.837350518729315,
  // },
  {
    x: -6053.2162531453,
    y: 2576.5176453853474,
    z: -2432.119994279658,
    rx: -2.246243023548608,
    ry: -0.7119031617936492,
    rz: -2.4575230805703376 + 2 * Math.PI,
  },
  {
    // x: -4823.801000774431,
    // y: 830.7806650445228,
    // z: -1491.3961944278508,
    // rx: -2.1514184314712272,
    // ry: -0.6068143001198765,
    // rz: -2.4260502657509253 + 2 * Math.PI,
    x: -5393.390519925912,
    y: 1166.3507799970637,
    z: -1825.0642758612064,
    rx: -1.899702636472065,
    ry: -0.9146709289486696,
    rz: -1.9775201936013262 + 2 * Math.PI,
  },
  {
    x: -12972.732062181243,
    y: 1692.961677306184,
    z: 501.6705574254255,
    rx: -0.8665012379038239,
    ry: -1.3197112319227395,
    rz: -0.8507430578004339 + 2 * Math.PI,
  },
];

var objectsToLoad = [
  {
    name: "se",
    children: [
      {
        name: "sala",
        path: "./models/divided/saladraco.glb",
        scale: [100, 100, 100],
      },
      {
        name: "sinoticos",
        path: "./models/divided/sinoticosdraco.glb",
        scale: [100, 100, 100],
      },
      {
        name: "setor1",
        path: "./models/divided/setor1draco.glb",
        scale: [100, 100, 100],
      },
      {
        name: "setor2",
        path: "./models/divided/setor2draco.glb",
        scale: [100, 100, 100],
      },
      {
        name: "towers",
        path: "./models/divided/towersdraco.glb",
        scale: [100, 100, 100],
      },
    ],
  },

  {
    name: "ortophoto",
    path: "./models/solo.fbx",
  },
  {
    name: "chaves",
    children: [
      {
        name: "sec1",
        path: "./models/sec1.fbx",
        position: [
          { x: -4378.6, y: 303, z: -1271 },
          { x: -4378.6, y: 303, z: -965 },
          { x: -4378.6, y: 303, z: -690 },
        ],
      },
      {
        name: "sec2",
        path: "./models/sec2.fbx",
        position: [
          { x: -4178.1, y: 303, z: -1271 },
          { x: -4178.1, y: 303, z: -965 },
          { x: -4178.1, y: 303, z: -690 },
        ],
      },
    ],
  },
  
  {
    name: "aviso",
    path: "Elemento interativo.text",
    scale: [1, 1, 1],
    position: [{ x: -4250, y: 400, z: -1000 }],
    rotation: [0, -Math.PI / 3 - Math.PI / 2, 0],
    tProperties: {
      height: 20,
      size: 70,
      hover: 10,
      curveSegments: 4,
      bevelThickness: 10,
      bevelSize: 1.5,
      bevelEnabled: true,
      font: undefined,
      fontName: "optimer",
      fontWeight: "bold",
      color: 0x993333,
      mirror: true,
      mirrorOpacity: 0.2,
    },
    onLoadAnimation: {
      duration: 100,
      objName: "aviso",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 0,
          target: "children",
          initialState: 1,
        },
        {
          property: "position",
          type: "absolute",
          value: [-4250, 0, -1000],
          target: "obj",
          initialState: [-4250, 400, -1000],
        },
        {
          property: "rotation",
          type: "absolute",
          value: [0, 0, 0],
          target: "obj",
          initialState: [0, -Math.PI / 3 - Math.PI / 2, 0],
        },
      ],
    },
  },
];

var animationKeys = {
  sec1: {
    open: {
      objName: "sec1",
      poseKeys: [
        {
          property: "rotation",
          type: "absolute",
          value: [0, Math.PI / 2, 0],
          target: "children",
        },
      ],
    },
    close: {
      objName: "sec1",
      poseKeys: [
        {
          property: "rotation",
          type: "absolute",
          value: [0, 0, 0],
          target: "children",
        },
      ],
    },
    highlightOn: {
      objName: "sec1",
      poseKeys: [
        {
          property: "EmissiveIntensity",
          type: "absolute",
          value: 1,
          color: 0xf00000,
          target: "children",
          initialState: 0,
        },
      ],
    },
    highlightOff: {
      objName: "sec1",
      poseKeys: [
        {
          property: "EmissiveIntensity",
          type: "absolute",
          value: 0,
          target: "children",
        },
      ],
    },
  },
  sec2: {
    open: {
      objName: "sec2",
      poseKeys: [
        {
          property: "rotation",
          type: "absolute",
          value: [0, -Math.PI / 2, 0],
          target: "children",
        },
      ],
    },
    close: {
      objName: "sec2",
      poseKeys: [
        {
          property: "rotation",
          type: "absolute",
          value: [0, 0, 0],
          target: "children",
        },
      ],
    },
    highlightOn: {
      objName: "sec2",
      poseKeys: [
        {
          property: "EmissiveIntensity",
          type: "absolute",
          value: 1,
          color: 0xf00000,
          target: "children",
          initialState: 0,
        },
      ],
    },
    highlightOff: {
      objName: "sec2",
      poseKeys: [
        {
          property: "EmissiveIntensity",
          type: "absolute",
          value: 0,
          target: "children",
        },
      ],
    },
  },
  se: {
    reduceOpacity: {
      objName: "se",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 0.5,
          target: "children",
        },
      ],
    },
    xray: {
      objName: "sala",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 0.3,
          target: "children",
        },
      ],
    },
    increaseOpacity: {
      objName: "sala",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 1,
          target: "children",
        },
      ],
    },
    showUp: {
      objName: "se",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 1,
          target: "children",
          initialState: 0,
        },
        {
          property: "position",
          type: "absolute",
          value: [0, 0, 0],
          target: "obj",
          initialState: [0, -400, 0],
        },
      ],
    },
  },
  ortophoto: {
    fromInvisible: {
      objName: "ortophoto",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 100,
          target: "children",
          initialState: 0,
        },
      ],
    },
  },
  sinoticos: {
    highlightOn: {
      objName: "sinoticos",
      poseKeys: [
        {
          property: "EmissiveIntensity",
          type: "absolute",
          value: 1,
          color: 0xf00000,
          target: "children",
          initialState: 0,
        },
      ],
    },
    highlightOff: {
      objName: "sinoticos",
      poseKeys: [
        {
          property: "EmissiveIntensity",
          type: "absolute",
          value: 0,
          target: "children",
        },
      ],
    },
  },
  setor1: {
    highlightOn: {
      objName: "setor1",
      poseKeys: [
        {
          property: "EmissiveIntensity",
          type: "absolute",
          value: 1,
          color: 0xf00000,
          target: "children",
          initialState: 0,
        },
      ],
    },
    highlightOff: {
      objName: "setor1",
      poseKeys: [
        {
          property: "EmissiveIntensity",
          type: "absolute",
          value: 0,
          target: "children",
        },
      ],
    },
  },
  setor2: {
    highlightOn: {
      objName: "setor2",
      poseKeys: [
        {
          property: "EmissiveIntensity",
          type: "absolute",
          value: 1,
          color: 0xf00000,
          target: "children",
          initialState: 0,
        },
      ],
    },
    highlightOff: {
      objName: "setor2",
      poseKeys: [
        {
          property: "EmissiveIntensity",
          type: "absolute",
          value: 0,
          target: "children",
        },
      ],
    },
  },
  aviso: {
    in: {
      objName: "aviso",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 1,
          target: "children",
        },
        {
          property: "position",
          type: "absolute",
          value: [-4250, 400, -1000],
          target: "obj",
        },
        {
          property: "rotation",
          type: "absolute",
          value: [0, -Math.PI / 3 - Math.PI / 2, 0],
          target: "obj",
        },
      ],
    },
    out: {
      objName: "aviso",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 0,
          target: "children",
        },
        {
          property: "position",
          type: "absolute",
          value: [-4250, 0, -1000],
          target: "obj",
        },
        {
          property: "rotation",
          type: "absolute",
          value: [0, 0, 0],
          target: "obj",
        },
      ],
    },
  },
  towers: {
    showDown: {
      objName: "towers",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 0,
          target: "children",
        },
        {
          property: "position",
          type: "absolute",
          value: [0, -400, 0],
          target: "obj",
        },
      ],
    },
    showUp: {
      objName: "towers",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 1,
          target: "children",
        },
        {
          property: "position",
          type: "absolute",
          value: [0, 0, 0],
          target: "obj",
        },
      ],
    },
  },
};

var controller;

init();

function init() {
  controller = ENVController;
  controller.positionsList = positionsList;
  controller.objectsToLoad = objectsToLoad;
  controller.clickableObjs = clickableObjs;
  controller.animationKeys = animationKeys;
  controller.createEnvironment();
  controller.freeCamera=true;
  controller.onObjClicked = onObjClicked;
  controller.onMouseOverObj = onObjOver;

  controller.onMouseOutOfObj = onObjOut;
}

function onObjOver (obj) {
    if (controller.verifyParentName(obj,"chaves")) {
     
 controller.animateKeys([
      animationKeys.sec1.highlightOn,
      animationKeys.sec2.highlightOn,
    ]);
      }
    }
 


  function onObjOut (obj) {
   
 controller.animateKeys([
      animationKeys.sec1.highlightOff,
      animationKeys.sec2.highlightOff,
    ]);
      }
 


function onObjClicked (obj) {
    if (controller.verifyParentName(obj,"chaves")) {
      if (opened) {
 controller.animateKeys([
      animationKeys.sec1.close,
      animationKeys.sec2.close,
    ]);
    
      } else {
 controller.animateKeys([
      animationKeys.sec1.open,
      animationKeys.sec2.open,
    ]);;
      }
   opened = !opened;
    }
  }